import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {ClassesHomeComponent} from './components/classes-home/classes-home.component';
import {ClassesCurrentComponent} from './components/classes-current/classes-current.component';
import {ClassesAllComponent} from './components/classes-all/classes-all.component';

const routes: Routes = [
  {
    path: '',
    component: ClassesHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'current'
      },
      {
        path: 'all',
        component: ClassesAllComponent
      },
      {
        path: 'current',
        component: ClassesCurrentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassesRoutingModule { }
