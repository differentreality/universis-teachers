import { Injectable, Component, OnInit } from '@angular/core';
import { ConfigurationService } from '../services/configuration.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-langswitch',
  templateUrl: './langswitch.component.html',
  styleUrls: ['./langswitch.component.scss']
})

export class LangswitchComponent {

    public currentLang = 'en';
  public languages: string[] = ['el', 'en', 'de'];
  // private defaultLanguage = 'en';
  constructor(private config: ConfigurationService, private translate: TranslateService) {
    // this.currentLang = this.config.getCurrentLang();
    // let configuration = this.config.settings;
      this.currentLang = translate.currentLang;
    this.languages = config.settings.localization.cultures;
    // this.languages = configuration.localization.cultures;
  }

  changeLang(lang) {
    console.log(lang);
    this.config.setCurrentLang(lang);
  }
}
