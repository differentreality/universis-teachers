import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-average-per-period',
  templateUrl: './average-per-period.component.html',
  styles: [`
      .table thead th { 
          border-bottom: 0 !important; 
      }
      .table th {
          border-top: 0 !important;
      }
  `]
})
export class AveragePerPeriodComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
